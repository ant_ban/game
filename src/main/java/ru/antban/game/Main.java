package ru.antban.game;

import java.util.Scanner;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.UserCommand;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.Scene;

public class Main {

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Game game = Game.build();
        Scene scene = new Scene(System.out);
        UserCommand command = parseCommand(null);
        System.out.println("Starting a game");
        while (game.executeAction(command, scene)) {
            scene.render();
            command = parseCommand(scanner.nextLine());
        }
        scene.render();
        System.out.println("Game finished, hope you enjoyed!");
    }

    private static UserCommand parseCommand(String command) {
        if (null == command) {
            return new UserCommand(null, null);
        } else {
            command = command.trim();
            final int separatorPos = command.indexOf(' ');
            if (separatorPos >= 0) {
                return new UserCommand(command.substring(0, separatorPos), command.substring(separatorPos + 1).trim());
            }
            return new UserCommand(command, null);
        }
    }
}
