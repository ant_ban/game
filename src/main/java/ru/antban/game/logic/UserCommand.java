package ru.antban.game.logic;

import java.util.stream.Stream;

public class UserCommand {
    public final String name;
    public final String parameter;

    public UserCommand(String name, String parameter) {
        this.name = name;
        this.parameter = parameter;
    }

    public boolean isEmpty() {
        return null == name;
    }

    public boolean isA(String... commands) {
        return Stream.of(commands).filter(cmd-> cmd.equalsIgnoreCase(name)).findAny().isPresent();
    }

    public String getParameter() {
        return parameter;
    }

    public int getIntParamter() throws GameException {
        try {
            return Integer.parseInt(parameter);
        } catch (NumberFormatException ex) {
            throw new GameException("Failed to parse integer parameter for " + name + "(" + parameter + ")", ex);
        }
    }

    @Override
    public String toString() {
        return "UserCommand{" +
                "name='" + name + '\'' +
                ", parameter='" + parameter + '\'' +
                '}';
    }
}
