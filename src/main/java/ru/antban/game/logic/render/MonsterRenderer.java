package ru.antban.game.logic.render;

import ru.antban.game.logic.obj.Monster;
import ru.antban.game.logic.obj.Point;

public class MonsterRenderer extends CharacterRenderer<Monster>{
    public MonsterRenderer(Monster character) {
        super(character);
    }

    @Override
    public int getZIndex() {
        return 1;
    }

    @Override
    protected int getLineCount() {
        return 0;
    }

    @Override
    protected void fillSpecific(int start, char[][] result) {
    }

    @Override
    public Point getPlace() {
        return new Point(CHAR_WIDTH * 2, 0);
    }
}
