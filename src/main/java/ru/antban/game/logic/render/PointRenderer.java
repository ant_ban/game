package ru.antban.game.logic.render;

import java.util.function.Supplier;
import ru.antban.game.logic.RenderObject;
import ru.antban.game.logic.obj.GameObject;
import ru.antban.game.logic.obj.HealthPack;
import ru.antban.game.logic.obj.Point;

public class PointRenderer implements RenderObject {
    private final Point point;
    private final Supplier<Character> symbolSupplier;
    private final char[][] image;
    private final int zIndex;

    public PointRenderer(Point point, char renderSymbol, int zIndex) {
        this(point, () -> renderSymbol, zIndex);
    }

    public PointRenderer(Point point, Supplier<Character> symbolSupplier, int zIndex) {
        this.point = point;
        this.image = new char[1][];
        this.image[0] = new char[1];
        this.symbolSupplier = symbolSupplier;
        this.zIndex = zIndex;
    }

    @Override
    public int getZIndex() {
        return zIndex;
    }

    @Override
    public Point getPlace() {
        return point;
    }

    @Override
    public char[][] render() {
        image[0][0] = symbolSupplier.get();
        return image;
    }

    public static PointRenderer build(GameObject obj) {
        switch (obj.getType()) {
            case HEALTH_PACK: {
                final HealthPack hp = (HealthPack) obj;
                return new PointRenderer(obj.getPosition(), () -> hp.getHealthLeft() > 0 ? 'H' : '-', 2);
            }
            case PLAYER:
                return new PointRenderer(obj.getPosition(), '*', 10);
            case MONSTER:
                return new PointRenderer(obj.getPosition(), 'M', 3);
            case FINISH:
                return new PointRenderer(obj.getPosition(), 'F', 4);
            default:
                throw new UnsupportedOperationException("Object type " + obj.getType() + " is not supported");
        }
    }
}
