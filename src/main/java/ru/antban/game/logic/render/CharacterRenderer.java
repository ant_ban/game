package ru.antban.game.logic.render;

import java.util.Arrays;
import ru.antban.game.logic.RenderObject;
import ru.antban.game.logic.obj.CharacterObject;

public abstract class CharacterRenderer<T extends CharacterObject> implements RenderObject {
    private char[][] image;
    private final T character;
    protected static final int CHAR_WIDTH = 15;

    protected CharacterRenderer(T character) {
        this.character = character;
    }

    protected abstract int getLineCount();

    protected T getCharacter() {
        return character;
    }

    protected abstract void fillSpecific(int start, char[][] result);

    @Override
    public final char[][] render() {
        if (null == image) {
            image = new char[getLineCount() + 4][];
            for (int i = 0; i < image.length; ++i) {
                image[i] = new char[CHAR_WIDTH];
            }
        }
        for (int i = 0; i < image.length; ++i) {
            Arrays.fill(image[i], ' ');
        }
        fill((character.getName() == null ? "" : character.getName()).toCharArray(), image[0]);
        fillParameter("Strength:", character.getStrength(), image[1]);
        fillParameter("Health:", character.getHealth(), image[2]);
        fillParameter("Luck:", character.getLuck(), image[3]);

        fillSpecific(4, image);
        return image;
    }

    protected static void fillParameter(String name, int parameter, char[] result) {
        fill(name.toCharArray(), result);
        fillFromBack(parameter, result);
    }

    protected static void fill(char[] src, char[] dst) {
        System.arraycopy(src, 0, dst, 0, Math.min(src.length, dst.length));
    }

    protected static void fillFromBack(int value, char[] dst) {
        int backPos = 1;
        dst[dst.length - 1] = '0';
        while (value != 0) {
            dst[dst.length - backPos] = (char) ('0' + (value % 10));
            value = value / 10;
            backPos++;
        }
    }

}
