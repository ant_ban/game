package ru.antban.game.logic.render;

import ru.antban.game.logic.obj.Player;
import ru.antban.game.logic.obj.Point;

public class PlayerRenderer extends CharacterRenderer<Player> {

    public PlayerRenderer(Player player) {
        super(player);
    }

    @Override
    public int getZIndex() {
        return 1000;
    }

    @Override
    protected int getLineCount() {
        return 2;
    }

    @Override
    protected void fillSpecific(int start, char[][] result) {
        fillParameter("Points:", getCharacter().getPointsLeft(), result[start]);
        fillParameter("Exp:", getCharacter().getExperience(), result[start + 1]);
    }

    @Override
    public Point getPlace() {
        return new Point(0, 0);
    }

}
