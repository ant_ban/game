package ru.antban.game.logic.render;

import java.util.Arrays;
import ru.antban.game.logic.GameMap;
import ru.antban.game.logic.obj.Point;
import ru.antban.game.logic.RenderObject;

public class MapRenderer implements RenderObject {
    private final GameMap gameMap;
    private final char[][] renderedPart;

    @Override
    public int getZIndex() {
        return 0;
    }

    public MapRenderer(GameMap gameMap) {
        this.gameMap = gameMap;
        this.renderedPart = new char[gameMap.getMap().length + 2][];
        for (int i = 0; i < renderedPart.length; ++i) {
            renderedPart[i] = new char[gameMap.getMap()[0].length + 2];
        }

    }

    private char renderItem(int i, int j) {
        int value = gameMap.getMap()[j][i];
        if ((value & GameMap.VISIBILITY_MASK) == 0) {
            return ' ';
        }
        value = value & ~GameMap.VISIBILITY_MASK;
        value = value & ~GameMap.VISIT_MASK;
        switch (value) {
            case GameMap.WALL:
                return 'W';
            case GameMap.ROAD:
                return '.';
        }
        return '?';
    }

    @Override
    public Point getPlace() {
        return new Point(-1, -1);
    }

    @Override
    public char[][] render() {
        drawLineInplace(renderedPart[0]);
        for (int i = 0; i < gameMap.getMap().length; ++i) {
            renderedPart[i + 1][0] = '|';
            for (int j = 0; j < gameMap.getMap()[i].length; ++j) {
                renderedPart[i + 1][j + 1] = renderItem(j, i);
            }
            renderedPart[i + 1][renderedPart[i + 1].length - 1] = '|';
        }
        drawLineInplace(renderedPart[renderedPart.length - 1]);
        return renderedPart;
    }

    private static void drawLineInplace(char[] chars) {
        Arrays.fill(chars, '-');
        chars[0] = '+';
        chars[chars.length - 1] = '+';
    }

}
