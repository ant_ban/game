package ru.antban.game.logic;

import java.io.Serializable;
import ru.antban.game.logic.obj.GameObject;

public interface Stage extends Serializable {
    boolean execute(UserCommand command, Scene scene, Game game);

    void onEnter(Scene scene, Game game);

    void onObjectVisible(Scene scene, GameObject object);
}
