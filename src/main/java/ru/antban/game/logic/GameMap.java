package ru.antban.game.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import ru.antban.game.logic.obj.CharacterObject;
import ru.antban.game.logic.obj.Point;

public class GameMap implements Serializable {
    public static final int NUM_MAP = 0xFF;
    public static final int VISIBILITY_MASK = 0x100;
    public static final int VISIT_MASK = 0x1000;
    private int[][] map;

    public boolean isInFinish(Point p) {
        return (map[p.getY()][p.getX()] & NUM_MAP) == FINISH;
    }

    public int[][] getMap() {
        return map;
    }

    public boolean move(CharacterObject character, int deltaX, int deltaY) {
        final Point characterPos = character.getPosition();
        final int newX = Math.min(Math.max(characterPos.x + deltaX, 0), map[0].length - 1);
        final int newY = Math.min(Math.max(characterPos.y + deltaY, 0), map.length - 1);
        if ((map[newY][newX] & NUM_MAP) == WALL) {
            return false;
        }
        characterPos.setX(newX);
        characterPos.setY(newY);
        refreshVisibility(characterPos);
        return true;
    }

    private void refreshVisibility(Point position) {
        setVisible(position.getX(), position.getY(), true);
        setVisible(position.getX() + 1, position.getY(), false);
        setVisible(position.getX() - 1, position.getY(), false);
        setVisible(position.getX(), position.getY() + 1, false);
        setVisible(position.getX(), position.getY() - 1, false);
    }

    private void setVisible(int x, int y, boolean visit) {
        if (x < 0 || x >= map[0].length) {
            return;
        }
        if (y < 0 || y >= map.length) {
            return;
        }
        if (visit) {
            map[y][x] = VISIBILITY_MASK | VISIT_MASK | map[y][x];
        } else {
            if ((map[y][x] & NUM_MAP) == WALL) {
                map[y][x] = VISIBILITY_MASK | map[y][x];
            }
        }
    }

    public void load(InputStream maze) throws GameException {
        final List<int[]> mazeLines = new ArrayList<>();
        try (final InputStreamReader isr = new InputStreamReader(maze, Charset.forName("UTF-8"));
             final BufferedReader mazeReader = new BufferedReader(isr)) {
            String line;
            Integer length = null;
            while (null != (line = mazeReader.readLine())) {
                line = line.trim();
                if (line.startsWith("#") || line.isEmpty()) {
                    continue;
                }
                if (null == length) {
                    length = line.length();
                } else if (length != line.length()) {
                    throw new GameException("Maze lines must be all the same length. First line: " + length + ", has line with length " + line.length());
                }
                mazeLines.add(lineToInt(line));
            }
            if (length == null) {
                throw new GameException("Maze is empty");
            }
        } catch (IOException ex) {
            throw new GameException("Failed to read maze");
        }
        final int[][] result = mazeLines.toArray(new int[mazeLines.size()][]);
        if (result[0][0] != ROAD) {
            throw new GameException("Incorrect maze: character is placed in (0,0), but there is no road there");
        }
        this.map = result;
    }

    private static final int MIN = 0;
    private static final int MAX = 4;

    public static final int ROAD = 0;
    public static final int WALL = 1;
    public static final int HEALTH = 2;
    public static final int MONSTER = 3;
    public static final int FINISH = 4;

    private static int[] lineToInt(String line) throws GameException {
        final char[] chars = line.toCharArray();
        final int[] result = new int[chars.length];
        for (int i = 0; i < chars.length; ++i) {
            final int value = chars[i] - '0';
            if (value < MIN || value > MAX) {
                throw new GameException("Incorrect number in maze: " + value);
            }
            result[i] = value;
        }
        return result;
    }

    public List<Point> replace(int code) {
        final List<Point> result = new ArrayList<>();
        for (int i= 0; i < map.length; ++i) {
            for (int j = 0; j < map[i].length; ++j) {
                if (map[i][j] == code) {
                    result.add(new Point(j, i));
                    map[i][j] = ROAD;
                }
            }
        }
        return result;
    }
}
