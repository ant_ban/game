package ru.antban.game.logic;

import ru.antban.game.logic.obj.Point;

public interface RenderObject {

    int getZIndex();

    Point getPlace();

    char[][] render();
}
