package ru.antban.game.logic;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import ru.antban.game.logic.cmd.GameCommand;
import ru.antban.game.logic.obj.FinishObject;
import ru.antban.game.logic.obj.GameObject;
import ru.antban.game.logic.obj.HealthPack;
import ru.antban.game.logic.obj.Monster;
import ru.antban.game.logic.obj.Player;
import ru.antban.game.logic.stage.CharacterConfigStage;
import ru.antban.game.logic.stage.InitStage;

public class Game implements Serializable {
    private Player player;
    private GameMap map;
    private final List<GameObject> objects = new ArrayList<>();
    private final List<Stage> stageStack = new ArrayList<>();

    public Game() {
        stageStack.add(new InitStage());
    }

    private Stage getTopStage() {
        return stageStack.isEmpty() ? null : stageStack.get(stageStack.size() - 1);
    }

    public boolean executeAction(UserCommand command, Scene scene) {
        final Stage topStage = getTopStage();
        if (null != topStage) {
            if (!topStage.execute(command, scene, this)) {
                stageStack.remove(topStage);
            }
            final Stage newTopStage = getTopStage();
            if (newTopStage != topStage &&  newTopStage != null) {
                newTopStage.onEnter(scene, this);
            }
        }
        return !stageStack.isEmpty();
    }

    public void addStage(Stage stage) {
        stageStack.add(stage);
    }

    public void clearStages() {
        stageStack.clear();
    }

    public void executeGameCommand(GameCommand gameCommand, Scene scene) throws GameException {
        gameCommand.execute(this, scene);
    }

    public GameMap getMap() {
        return map;
    }

    public Player getPlayer() {
        return player;
    }

    public void addExperience(int value) {
        getPlayer().addExperience(value);
        if (player.getPointsLeft() > 0) {
            addStage(new CharacterConfigStage(true));
        }
    }

    public void loadMap(String resource, Scene scene) throws GameException {
        final InputStream in = InitStage.class.getClassLoader().getResourceAsStream(resource);
        if (null == in) {
            throw new GameException("Failed to open maze file: " + resource);
        }
        try {
            map.load(in);
        } finally {
            try {
                in.close();
            } catch (IOException ingore) {
                // Ignore this exception
            }
        }
        objects.clear();
        objects.addAll(map.replace(GameMap.MONSTER).stream()
                .map(point -> Monster.build("Monster", point)).collect(Collectors.toList()));
        objects.addAll(map.replace(GameMap.HEALTH).stream()
                .map(point -> new HealthPack(point, 20)).collect(Collectors.toList()));
        objects.addAll(map.replace(GameMap.FINISH).stream()
                .map(FinishObject::new).collect(Collectors.toList()));
        player.getPosition().setX(0);
        player.getPosition().setY(0);
        move(scene, 0, 0);
    }

    public void replaceGame(Game restored) throws GameException {
        this.player = restored.player;
        this.map = restored.map;
        this.stageStack.clear();
        this.stageStack.addAll(restored.stageStack);
        this.objects.clear();
        this.objects.addAll(restored.objects);
    }

    public static Game build() {
        final Game result = new Game();
        result.player = Player.build();
        result.map = new GameMap();
        return result;
    }

    public GameObject findGameObject() {
        return objects.stream().filter(m -> m.getPosition().equals(player.getPosition())).findAny().orElse(null);
    }

    public List<GameObject> getVisibleObjects() {
        return objects.stream().filter(GameObject::isVisible).collect(Collectors.toList());
    }

    public boolean move(Scene scene, int deltaX, int deltaY) {
        final boolean success = getMap().move(player, deltaX, deltaY);
        if (success) {
            final GameObject obj = findGameObject();
            if (null != obj) {
                makeVisible(scene, obj);
            }
        }
        return success;
    }

    private void makeVisible(Scene scene, GameObject object) {
        if (!object.isVisible()) {
            object.setVisible(true);
            getTopStage().onObjectVisible(scene, object);
        }
    }

    public void makeAllVisible(Scene scene) {
        objects.forEach(o -> makeVisible(scene, o));
    }
}
