package ru.antban.game.logic.stage;

import java.util.ArrayList;
import java.util.List;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.Stage;
import ru.antban.game.logic.UserCommand;
import ru.antban.game.logic.cmd.ExitCommand;
import ru.antban.game.logic.cmd.LoadGameCommand;
import ru.antban.game.logic.cmd.SaveGameCommand;
import ru.antban.game.logic.obj.GameObject;

public abstract class GenericStage implements Stage {

    public static final String CMD_EXIT = "exit";

    @Override
    public final boolean execute(UserCommand command, Scene scene, Game game) {
        try {
            if (command.isEmpty()) {
                scene.addInfo("You are in abstract stage based game. Anyone can do anything. Type help for help");
                return true;
            } else if (command.isA("help")) {
                scene.addInfo("Commands:\n\n" + String.join("\n", enumerateCommands()));
                return true;
            } else if (command.isA(CMD_EXIT)) {
                game.executeGameCommand(new ExitCommand(), scene);
                return false;
            } else if (command.isA("load")) {
                game.executeGameCommand(new LoadGameCommand(command.getParameter()), scene);
                return false; //close this stage.
            } else if (command.isA("save")) {
                game.executeGameCommand(new SaveGameCommand(command.getParameter()), scene);
                return true;
            } else if (command.isA("debug")) {
                if ("on".equalsIgnoreCase(command.getParameter())) {
                    scene.setDebug(true);
                    scene.addInfo("Debug mode is ON");
                } else if ("off".equalsIgnoreCase(command.getParameter())) {
                    scene.setDebug(false);
                    scene.addInfo("Debug mode is OFF");
                } else {
                    scene.addInfo("Debug mode must be ON or OFF. You tried to set it to " + command.getParameter());
                }
                return true;
            } else {
                return executeInternal(command, scene, game);
            }
        } catch (GameException ex) {
            scene.addError(ex);
        }
        return true;
    }

    protected abstract boolean executeInternal(UserCommand command, Scene scene, Game game) throws GameException;

    @Override
    public void onObjectVisible(Scene scene, GameObject object) {
        // do nothing
    }

    protected List<String> enumerateCommands() {
        final List<String> result = new ArrayList<>();
        result.add(CMD_EXIT + "\t - exit from game");
        result.add("help\t - print information about available commands");
        result.add("load <filename>\t - load game from file <filename>");
        result.add("save <filename>\t - save game to file <filename>");
        result.add("debug on|off\t - turn on/off debugging information on errors");
        return result;
    }
}
