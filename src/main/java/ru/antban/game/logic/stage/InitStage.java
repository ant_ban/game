package ru.antban.game.logic.stage;

import java.util.List;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.UserCommand;
import ru.antban.game.logic.cmd.StartGameCommand;

public class InitStage extends GenericStage {
    @Override
    public boolean executeInternal(UserCommand command, Scene scene, Game game) throws GameException {
        if (command.isA("character")) {
            game.addStage(new CharacterConfigStage(false));
            return true;
        } else if (command.isA("start")) {
            game.executeGameCommand(new StartGameCommand(), scene);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected List<String> enumerateCommands() {
        final List<String> result = super.enumerateCommands();
        result.add("character\t - create/modify your character");
        result.add("start\t - start a game");
        return result;
    }

    @Override
    public void onEnter(Scene scene, Game game) {
        scene.addInfo("You are now are still not in game (some kind of menu).\n" +
                "Configure your character with 'character' command and type 'start' to begin");
        scene.clearRenderedObjects();
    }
}
