package ru.antban.game.logic.stage;

import java.util.List;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.UserCommand;
import ru.antban.game.logic.cmd.MoveCharacterCommand;
import ru.antban.game.logic.obj.GameObject;
import ru.antban.game.logic.render.MapRenderer;
import ru.antban.game.logic.render.PointRenderer;

public class MapStage extends GenericStage {
    @Override
    protected boolean executeInternal(UserCommand command, Scene scene, Game game) throws GameException {
        if (command.isA("move") || command.isA("m")) {
            if (null != command.getParameter()) {
                game.executeGameCommand(new MoveCharacterCommand(command.getParameter().toCharArray()), scene);
            } else {
                scene.addInfo("Specify parameter - where to move.");
            }
        } else if (command.isA("test_all")) {
            game.makeAllVisible(scene);
        }
        return true;
    }

    @Override
    protected List<String> enumerateCommands() {
        final List<String> result = super.enumerateCommands();
        result.add("move <directions>\t - move up(w) left(a) down(s) right(d)");
        return result;
    }

    @Override
    public void onEnter(Scene scene, Game game) {
        scene.clearRenderedObjects();
        scene.addRenderedObject(new MapRenderer(game.getMap()));
        scene.addRenderedObject(PointRenderer.build(game.getPlayer()));
        game.getVisibleObjects().stream().map(PointRenderer::build).forEach(scene::addRenderedObject);
    }

    @Override
    public void onObjectVisible(Scene scene, GameObject object) {
        super.onObjectVisible(scene, object);
        scene.addInfo("You have found a " + object.getType());
        scene.addRenderedObject(PointRenderer.build(object));
    }
}
