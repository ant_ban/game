package ru.antban.game.logic.stage;

import java.util.List;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.UserCommand;
import ru.antban.game.logic.cmd.FightCommand;
import ru.antban.game.logic.obj.Monster;
import ru.antban.game.logic.render.MonsterRenderer;
import ru.antban.game.logic.render.PlayerRenderer;

public class BattleStage extends GenericStage {
    private final Monster monster;
    private int totalDamage = 0;

    public BattleStage(Monster monster) {
        this.monster = monster;
    }

    @Override
    protected boolean executeInternal(UserCommand command, Scene scene, Game game) throws GameException {
        if (command.isA("fight") || command.isA("f")) {
            final FightCommand cmd = new FightCommand(monster, totalDamage);
            game.executeGameCommand(cmd, scene);
            totalDamage = cmd.getTotalDamage();
            if (monster.isDefeated()) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected List<String> enumerateCommands() {
        final List<String> result = super.enumerateCommands();
        result.add("fight\t - try to make damage to a " + monster.getName());
        return result;
    }

    @Override
    public void onEnter(Scene scene, Game game) {
        scene.addInfo("Oh mine God! You have faced to a Monster: " + monster.getName());
        scene.clearRenderedObjects();
        scene.addRenderedObject(new PlayerRenderer(game.getPlayer()));
        scene.addRenderedObject(new MonsterRenderer(monster));
    }
}
