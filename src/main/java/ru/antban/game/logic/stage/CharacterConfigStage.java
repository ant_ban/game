package ru.antban.game.logic.stage;

import java.util.List;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.UserCommand;
import ru.antban.game.logic.render.PlayerRenderer;

public class CharacterConfigStage extends GenericStage {
    private boolean mustVerify;

    public CharacterConfigStage(boolean mustVerify) {
        this.mustVerify = mustVerify;
    }

    @Override
    protected boolean executeInternal(UserCommand command, Scene scene, Game game) throws GameException {
        if (command.isA("show")) {
            scene.addInfo("Your character has strength, health and luck. \npointsLeft - amount of points that must be converted to that parameters.");
        } else if (command.isA("name")) {
            game.getPlayer().setName(command.getParameter());
        } else if (command.isA("strength")) {
            game.getPlayer().setStrength(command.getIntParamter());
        } else if (command.isA("luck")) {
            game.getPlayer().setLuck(command.getIntParamter());
        } else if (command.isA("health")) {
            game.getPlayer().setHealth(command.getIntParamter());
        } else if (command.isA("test")) {
            game.getPlayer().setStrength(50);
            game.getPlayer().setHealth(100);
            game.getPlayer().setLuck(100);
            game.getPlayer().setName("Dmitry");
            game.getPlayer().verify();
            return false;
        } else if (command.isA("complete")) {
            if (mustVerify) {
                game.getPlayer().verify();
            }
            return false;
        } else {
            scene.addInfo("Unknown command " + command);
        }
        return true;
    }

    @Override
    protected List<String> enumerateCommands() {
        final List<String> result = super.enumerateCommands();
        result.add("name\t - Set character name");
        result.add("strength\t - Set character strength");
        result.add("luck\t - Set character strength");
        result.add("health\t - Set character strength");
        result.add("complete\t - Complete character configuration");
        return result;
    }

    @Override
    public void onEnter(Scene scene, Game game) {
        scene.addInfo("You are on character edit page. Type name to set name");
        scene.clearRenderedObjects();
        scene.addRenderedObject(new PlayerRenderer(game.getPlayer()));
    }
}
