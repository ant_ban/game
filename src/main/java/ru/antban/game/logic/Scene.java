package ru.antban.game.logic;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import ru.antban.game.logic.obj.Point;

public class Scene {
    private final PrintStream out;
    private List<String> info = new ArrayList<>();
    private List<String> errors = new ArrayList<>();
    private boolean debug = false;
    private List<RenderObject> renderedObjects = new ArrayList<>();

    public Scene(PrintStream out) {
        this.out = out;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public void render() {
        if (!renderedObjects.isEmpty()) {
            final List<Point> positions = renderedObjects.stream()
                    .map(RenderObject::getPlace).collect(Collectors.toList());
            final Point minPosition = new Point(
                    positions.stream().mapToInt(Point::getX).min().getAsInt(),
                    positions.stream().mapToInt(Point::getY).min().getAsInt());
            final List<char[][]> images = renderedObjects.stream()
                    .map(RenderObject::render).collect(Collectors.toList());
            int maxY = 0;
            int maxX = 0;
            for (int i = 0; i < images.size(); ++i) {
                final char[][] image = images.get(i);
                final Point position = positions.get(i);
                if ((image.length + position.getY()) > maxY) {
                    maxY = image.length + position.getY();
                }
                if (image.length > 0) {
                    if ((image[0].length + position.getX()) > maxX) {
                        maxX = image[0].length + position.getX();
                    }
                }
            }
            final char[][] renderArray = new char[maxY - minPosition.getY()][];
            for (int i = 0; i < renderArray.length; ++i) {
                renderArray[i] = new char[maxX - minPosition.getX()];
                Arrays.fill(renderArray[i], ' ');
            }
            for (int i = 0; i < images.size(); ++i) {
                final char[][] image = images.get(i);
                final Point delta = positions.get(i);
                for (int j = 0; j < image.length; ++j) {
                    System.arraycopy(
                            image[j],
                            0,
                            renderArray[j + delta.getY() - minPosition.getY()],
                            delta.getX() - minPosition.getX(),
                            image[j].length);
                }
            }
            out.println("===RENDER SCENE===");
            Stream.of(renderArray).map(String::new).forEach(out::println);
            out.println("===END SCENE===");
        }
        if (!info.isEmpty()) {
            out.println("===FYI===");
            info.forEach(out::println);
        }
        if (!errors.isEmpty()) {
            out.println("ERROR!!!!!");
            errors.forEach(out::println);
        }

        info.clear();
        errors.clear();
    }

    public void addInfo(String s) {
        info.add(s);
    }

    public void addError(GameException ex) {
        errors.add(ex.getMessage());
        if (debug) {
            ex.printStackTrace();
        }
    }

    public void resetInfos() {
        this.info.clear();
        this.errors.clear();
    }

    public void addRenderedObject(RenderObject obj) {
        renderedObjects.add(obj);
        renderedObjects.sort(Comparator.comparing(RenderObject::getZIndex));
    }

    public void clearRenderedObjects() {
        renderedObjects.clear();
    }
}
