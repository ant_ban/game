package ru.antban.game.logic.obj;

public class FinishObject extends GameObject{

    public FinishObject(Point position) {
        super(Type.FINISH, position);
    }
}
