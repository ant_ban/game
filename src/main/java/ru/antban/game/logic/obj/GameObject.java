package ru.antban.game.logic.obj;

import java.io.Serializable;

public class GameObject implements Serializable {

    public enum Type {
        MONSTER,
        PLAYER,
        HEALTH_PACK,
        FINISH,
    }

    private final Point position;
    private final Type type;
    private boolean visible;

    public GameObject(Type type, Point position) {
        this.position = position;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Point getPosition() {
        return position;
    }
}
