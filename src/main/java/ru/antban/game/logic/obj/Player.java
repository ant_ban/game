package ru.antban.game.logic.obj;

import ru.antban.game.logic.GameException;

public class Player extends CharacterObject {
    private static int MAX_PARAM = 100;

    private int pointsLeft;
    private int experience;
    private int experienceUsed;

    public Player() {
        super(Type.PLAYER, new Point(0, 0));
    }

    public void verify() throws GameException {
        if (getName() == null || getName().isEmpty()) {
            throw new GameException("Character name can't be empty");
        } else if (pointsLeft > 0) {
            throw new GameException("Can't continue with bonus points. They are all must be updated");
        }
    }

    public void setStrength(int strength) throws GameException {
        verifyAndRemovePoints("Strength", getStrength(), strength);
        init(strength, getHealth(), getLuck());
    }

    public void setHealth(int health) throws GameException {
        verifyAndRemovePoints("Health", getHealth(), health);
        init(getStrength(), health, getLuck());
    }

    public void setLuck(int luck) throws GameException {
        verifyAndRemovePoints("Luck", getLuck(), luck);
        init(getStrength(), getHealth(), luck);
    }

    public int getPointsLeft() {
        return pointsLeft;
    }

    public void setPointsLeft(int pointsLeft) {
        this.pointsLeft = pointsLeft;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void addExperience(int value) {
        this.experience += value;
        if (this.experience - this.experienceUsed >= 10) {
            final int convertToPoints = 10 * ((this.experience - this.experienceUsed) / 10);
            this.pointsLeft += convertToPoints;
            this.experienceUsed += convertToPoints;
        }
    }

    private void verifyAndRemovePoints(String name, int curVal, int newVal) throws GameException {
        final int totalPoints = getStrength() + getHealth() + getExperience() + getPointsLeft();
        if (totalPoints <= MAX_PARAM * 3) {
            if (newVal > MAX_PARAM || newVal < MIN_PARAM) {
                throw new GameException(name + " must be within interval [" + MIN_PARAM + ", " + MAX_PARAM + "], you are trying to set " + newVal);
            }
            if ((newVal - curVal) > pointsLeft) {
                throw new GameException("Only " + pointsLeft + " bonus points left. Can not set " + name + "[" + curVal + "] to " + newVal);
            }
        }
        pointsLeft -= newVal - curVal;
    }

    public static Player build() {
        final Player result = new Player();
        result.pointsLeft = MIN_PARAM * 2;
        result.init(MIN_PARAM, MIN_PARAM, MIN_PARAM);
        result.experience = 0;
        return result;
    }

    public int eatHealth(HealthPack health) {
        final int maxEat = MAX_PARAM - getHealth();
        if (maxEat > 0) {
            final int toEat = Math.min(maxEat, health.getHealthLeft());
            init(getStrength(), getHealth() + toEat, getLuck());
            health.setHealthLeft(health.getHealthLeft() - maxEat);
            return toEat;
        }
        return 0;
    }
}
