package ru.antban.game.logic.obj;

public class CharacterObject extends GameObject {
    protected static int MIN_PARAM = 50;

    private String name;
    private int strength;
    private int health;
    private int luck;

    protected CharacterObject(Type type, Point position) {
        super(type, position);
    }

    protected void init(int strength, int health, int luck) {
        this.strength = strength;
        this.health = health;
        this.luck = luck;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStrength() {
        return strength;
    }

    public int getHealth() {
        return health;
    }

    public int getLuck() {
        return luck;
    }

    public int takeDamage(int healthToTake) {
        final int oldHealth = this.health;
        this.health = Math.max(this.health - healthToTake, 0);
        return oldHealth - this.health;
    }
}
