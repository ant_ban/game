package ru.antban.game.logic.obj;

public class HealthPack extends GameObject {
    private int healthLeft;
    private boolean visible;

    public HealthPack(Point position, int healthLeft) {
        super(Type.HEALTH_PACK, position);
        this.healthLeft = healthLeft;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setHealthLeft(int healthLeft) {
        this.healthLeft = healthLeft;
    }

    public int getHealthLeft() {
        return healthLeft;
    }
}
