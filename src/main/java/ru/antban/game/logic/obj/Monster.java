package ru.antban.game.logic.obj;

public class Monster extends CharacterObject {

    public Monster(Point position) {
        super(Type.MONSTER, position);
    }

    public boolean isDefeated() {
        return getHealth() <= 0;
    }

    public static Monster build(String name, Point point) {
        final Monster result = new Monster(point);
        result.init(MIN_PARAM / 2, MIN_PARAM / 2, MIN_PARAM);
        result.setName(name);
        return result;
    }
}
