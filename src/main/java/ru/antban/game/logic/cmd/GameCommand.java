package ru.antban.game.logic.cmd;

import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;

public interface GameCommand {
    void execute(Game game, Scene scene) throws GameException;
}
