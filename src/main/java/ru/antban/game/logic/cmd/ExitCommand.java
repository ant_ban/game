package ru.antban.game.logic.cmd;

import ru.antban.game.logic.Game;
import ru.antban.game.logic.Scene;

public class ExitCommand implements GameCommand {
    @Override
    public void execute(Game game, Scene scene) {
        scene.addInfo("Buy buy...");
        game.clearStages();
    }
}
