package ru.antban.game.logic.cmd;

import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.obj.GameObject;
import ru.antban.game.logic.obj.HealthPack;
import ru.antban.game.logic.obj.Monster;
import ru.antban.game.logic.stage.BattleStage;

public class MoveCharacterCommand implements GameCommand {
    private final char[] moves;

    public MoveCharacterCommand(char[] moves) {
        this.moves = moves;
    }

    @Override
    public void execute(Game game, Scene scene) throws GameException {
        for (char ch : moves) {
            boolean moveSuccessfull = true;
            if (ch == 'w') {
                moveSuccessfull = game.move(scene, 0, -1);
            } else if (ch == 'a') {
                moveSuccessfull = game.move(scene, -1, 0);
            } else if (ch == 's') {
                moveSuccessfull = game.move(scene, 0, 1);
            } else if (ch == 'd') {
                moveSuccessfull = game.move(scene, 1, 0);
            } else {
                scene.addInfo("Unknown move code " + ch);
            }

            if (!moveSuccessfull) {
                scene.addInfo("Failed to move - probably wall is on the road");
                break;
            } else {
                final GameObject object = game.findGameObject();
                if (null != object) {
                    switch (object.getType()) {
                        case MONSTER: {
                            final Monster m = (Monster) object;
                            if (!m.isDefeated()) {
                                game.addStage(new BattleStage(m));
                                return;
                            }
                            break;
                        }
                        case HEALTH_PACK: {
                            final HealthPack hp = (HealthPack) object;
                            if (hp.getHealthLeft() > 0) {
                                scene.addInfo("You are on a health pack. Health left: " + hp.getHealthLeft());
                                final int eat = game.getPlayer().eatHealth(hp);
                                scene.addInfo("You eat " + eat + " health");
                            } else {
                                scene.addInfo("There was a health pack here. But no health left...");
                            }
                            break;
                        }
                        case FINISH: {
                            scene.addInfo("Congratulations! You have found the purpose of life! EXITING! ");
                            game.executeGameCommand(new ExitCommand(), scene);
                            return;
                        }
                    }
                }
            }
        }
    }
}
