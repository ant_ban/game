package ru.antban.game.logic.cmd;

import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.stage.MapStage;

public class StartGameCommand implements GameCommand{
    @Override
    public void execute(Game game, Scene scene) throws GameException {
        game.getPlayer().verify();
        game.loadMap("maze.lab", scene);
        game.addStage(new MapStage());
    }
}
