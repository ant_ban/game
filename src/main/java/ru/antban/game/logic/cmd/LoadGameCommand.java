package ru.antban.game.logic.cmd;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;

public class LoadGameCommand implements GameCommand {
    private final String fileName;

    public LoadGameCommand(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void execute(Game game, Scene scene) throws GameException {
        scene.addInfo("Loading from file " + fileName);
        if (fileName == null) {
            throw new GameException("File name is not set!");
        }
        try (InputStream in = new FileInputStream(fileName)) {
            final Game restored = (Game) new ObjectInputStream(in).readObject();
            game.replaceGame(restored);
        } catch (IOException | ClassNotFoundException ex) {
            throw new GameException("Failed to load file", ex);
        }
        scene.addInfo("Successfully load from " + fileName);
    }
}
