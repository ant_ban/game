package ru.antban.game.logic.cmd;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;

public class SaveGameCommand implements GameCommand {
    private final String fileName;

    public SaveGameCommand(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void execute(Game game, Scene scene) throws GameException {
        scene.addInfo("Saving to file " + fileName);
        if (fileName == null) {
            throw new GameException("File name is not set!");
        }
        try (OutputStream out = new FileOutputStream(fileName)) {
            final ObjectOutputStream objectStream = new ObjectOutputStream(out);
            objectStream.writeObject(game);
        } catch (IOException ex) {
            throw new GameException("Failed to write to file", ex);
        }
        scene.addInfo("Game successfully saved");
    }
}
