package ru.antban.game.logic.cmd;

import java.util.Random;
import ru.antban.game.logic.Game;
import ru.antban.game.logic.GameException;
import ru.antban.game.logic.Scene;
import ru.antban.game.logic.obj.CharacterObject;
import ru.antban.game.logic.obj.Monster;

public class FightCommand implements GameCommand {
    private final Monster monster;
    private static final Random rnd = new Random();
    private int totalDamage;

    public FightCommand(Monster monster, int totalDamage) {
        this.monster = monster;
        this.totalDamage = totalDamage;
    }

    public int getTotalDamage() {
        return totalDamage;
    }

    @Override
    public void execute(Game game, Scene scene) throws GameException {
        // Well.. won't make big deal from logic here. According to luck will select the one who will beat.
        final boolean playerAttacks = rnd.nextInt(game.getPlayer().getLuck() + monster.getLuck()) < game.getPlayer().getLuck();
        if (playerAttacks) {
            scene.addInfo("You have a good luck! You will beat the " + monster.getName());
        } else {
            scene.addInfo("Bad luck. Monster attacks first");
        }
        final CharacterObject attacker = playerAttacks ? game.getPlayer() : monster;
        final CharacterObject receiver = playerAttacks ? monster : game.getPlayer();
        final int strength = rnd.nextInt(attacker.getStrength());
        final int damage = receiver.takeDamage(strength);
        totalDamage += damage;

        scene.addInfo(attacker.getName() + " attacks " + receiver.getName() + " with strength " + strength + ". " + receiver.getName() + " takes " + damage + " damage");
        if (receiver.getHealth() < 0) {
            scene.addInfo(receiver.getName() + " is dead now");
            if (playerAttacks) {
                final int points = totalDamage / 2;
                scene.addInfo("Well done! you killed a " + receiver.getName() + " and gain " + points + " experience points");
                game.addExperience(points);
            } else {
                scene.addInfo("Well... embracing. " + attacker.getName() + " defeated you. Try again later.");
                game.executeGameCommand(new ExitCommand(), scene);
            }
        }
    }
}
