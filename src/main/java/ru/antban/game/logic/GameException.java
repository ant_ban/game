package ru.antban.game.logic;

public class GameException extends Exception {
    public GameException(String message) {
        super(message);
    }

    public GameException(String message, Exception cause) {
        super(message, cause);
    }
}
